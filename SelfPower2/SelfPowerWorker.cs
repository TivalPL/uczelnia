﻿using System;
using System.Numerics;
using System.Threading;


namespace SelfPower2
{
    public class SelfPowerWorker
    {
        /// <summary>
        /// Create self power sum of given number
        /// </summary>
        /// <param name="maxPower">Maximum power</param>
        public BigInteger CreateSelfPowerSum(int maxPower)
        {
            var powerSum = new BigInteger();

            for (int i = 1; i <= maxPower; i++)
                powerSum += (BigInteger)Math.Pow(i, i);

            return powerSum;
        }

        /// <summary>
        /// Cuts a specific number of digits of a number
        /// </summary>
        /// <param name="powerSum">Number to cute</param>
        /// <param name="digitsCount">How many digits will be cute</param>
        public string GetLastDigits(BigInteger powerSum, int digitsCount)
        {
            var stringPowerSum = powerSum.ToString();
            var lengthPowerSum = stringPowerSum.Length;

            if (lengthPowerSum < digitsCount)
                return string.Empty;

            return stringPowerSum.Substring(lengthPowerSum - digitsCount, digitsCount);
        }
    }
}
