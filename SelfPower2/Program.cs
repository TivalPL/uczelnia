﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;


namespace SelfPower2
{
    class Program
    {
        static void Main(string[] args)
        {
            var selfPowerWorker = new SelfPowerWorker();

            var selfPower = selfPowerWorker.CreateSelfPowerSum(10);
            var lastTenDigits = selfPowerWorker.GetLastDigits(selfPower, 15);

            var currentTime = DateTime.Now.Millisecond;
            var result = selfPowerWorker.CreateSelfPowerSum(20);
            var endTime = DateTime.Now.Millisecond;

            var deltaTime = endTime - currentTime;
        }
    }
}
