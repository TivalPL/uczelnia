﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SelfPower2;

namespace SelfPower2UnitTest
{
    [TestClass]
    public class SelfPowerWorkerTest
    {
        SelfPowerWorker spw = new SelfPowerWorker();
        [TestMethod]
        public void SelfPowerSum()
        {
            var result = spw.CreateSelfPowerSum(10);
            Assert.AreEqual(10405071317, result);
            //dodam komentarz
        }

        [TestMethod]
        public void GetLastDigits()
        {
            var result = spw.GetLastDigits(123456789, 5);
            Assert.AreEqual("56789", result);
        }
    }
}
